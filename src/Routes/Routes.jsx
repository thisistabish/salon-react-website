import { createBrowserRouter } from 'react-router-dom'
import Home from '../Components/Home_Page/Home'
import Services from '../Components/Service_Page/Services'

const router = createBrowserRouter ([
    {
    path : "/",
    element : <Home/>,

    },
    {
        path : "/services",
        element : <Services/>,
    
        }
]);
  

export default router