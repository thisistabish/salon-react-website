import React from 'react'



import './HeroSection.css'



const HeroSection = ({route}) => {
  return (
    <div className='container-hero m-auto flex justify-center ' style={{ backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url(${route?.heroSection?.background})` }}>
    

    <div className='banner-heading max-w-screen-xl '>
      <h1 className='font-2' >{route?.heroSection?.title}</h1>
      <h2>{route?.heroSection?.subTitle}</h2>
      <button >DISCOVER MORE</button>
    </div>
    </div>
  )
}

export default HeroSection