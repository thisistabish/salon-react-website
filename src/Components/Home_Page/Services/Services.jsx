import React from 'react'
import "./Services.css"
import Button from '../Button/Button'

const Services = () => {
  return (
   
    <div style={{backgroundColor:"#f8f5f2", paddingTop:"25px"}}>
    <div className='boxed-width' >
        <div className='service-content'>
            <h4>FOCUS ON BEAUTY</h4>
            <h1 className='font-2'>Our Service Menu</h1>
            <div className='grid'>
                <div className='grid-column'>
                    <div className='service-detail'>
                        <h2>Herbal Facial</h2>
                        <span className='dashes'></span>
                        <span>$75</span>
                    </div>
                    <div className='timing'><h2>Service length 1 hour</h2></div>
                    <div className='service-detail'>
                        <h2>Herbal Facial</h2>
                        <span className='dashes'></span>
                        <span>$75</span>
                    </div>
                    <div className='timing'><h2>Service length 1 hour</h2></div>
                    <div className='service-detail'>
                        <h2>Herbal Facial</h2>
                        <span className='dashes'></span>
                        <span>$75</span>
                    </div>
                    <div className='timing'><h2>Service length 1 hour</h2></div>
                    <div className='service-detail'>
                        <h2>Herbal Facial</h2>
                        <span className='dashes'></span>
                        <span>$75</span>
                    </div>
                    <div className='timing'><h2>Service length 1 hour</h2></div>
                    <div className='service-detail'>
                        <h2>Herbal Facial</h2>
                        <span className='dashes'></span>
                        <span>$75</span>
                    </div>
                    <div className='timing'><h2>Service length 1 hour</h2></div>
                    <div className='service-detail'>
                        <h2>Herbal Facial</h2>
                        <span className='dashes'></span>
                        <span>$75</span>
                    </div>
                    <div className='timing'><h2>Service length 1 hour</h2></div>


                </div>
                <div className='grid-column'>
                <div className='service-detail'>
                        <h2>Herbal Facial</h2>
                        <span className='dashes'></span>
                        <span>$75</span>
                    </div>
                    <div className='timing'><h2>Service length 1 hour</h2></div>
                    <div className='service-detail'>
                        <h2>Herbal Facial</h2>
                        <span className='dashes'></span>
                        <span>$75</span>
                    </div>
                    <div className='timing'><h2>Service length 1 hour</h2></div>
                    <div className='service-detail'>
                        <h2>Herbal Facial</h2>
                        <span className='dashes'></span>
                        <span>$75</span>
                    </div>
                    <div className='timing'><h2>Service length 1 hour</h2></div>
                    <div className='service-detail'>
                        <h2>Herbal Facial</h2>
                        <span className='dashes'></span>
                        <span>$75</span>
                    </div>
                    <div className='timing'><h2>Service length 1 hour</h2></div>
                    <div className='service-detail'>
                        <h2>Herbal Facial</h2>
                        <span className='dashes'></span>
                        <span>$75</span>
                    </div>
                    <div className='timing'><h2>Service length 1 hour</h2></div>
                    <div className='service-detail'>
                        <h2>Herbal Facial</h2>
                        <span className='dashes'></span>
                        <span>$75</span>
                    </div>
                    <div className='timing'><h2>Service length 1 hour</h2></div>


                </div>
            </div>
        </div>
        <div style={{textAlign:"center"}}><Button  text="View All Prices"/></div>
    </div>
    </div>
    
  )
}

export default Services