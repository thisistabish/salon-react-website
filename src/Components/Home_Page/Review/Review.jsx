import React from 'react'
import './Review.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { faStar as regularStar } from '@fortawesome/free-regular-svg-icons';
import Slider from "react-slick";
const Review = () => {
  var ratings ={
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay:true
  }

  return (
    <div className='review-container '>
        <h4>TESTIMONIALS</h4>
        <h1 className='font-2'>Comments & Reviews</h1>
        <Slider {...ratings } className='boxed-width review-grid'>
       
        
            <div>
            <div className='review-1'>
            <FontAwesomeIcon icon={faStar} className='star-color'/>
            <FontAwesomeIcon icon={faStar} className='star-color'/>
            <FontAwesomeIcon icon={faStar} className='star-color'/>
            <FontAwesomeIcon icon={faStar} className='star-color'/>
            <FontAwesomeIcon icon={regularStar} className='star-color'/>
            <p>Etiam sapien sagittis diam congue augue massa varius egestas ociis ultrice varius 
						magna tempus an aliquet undo cursus suscipit augue pretium lacinia	</p>
            </div>
            </div>
            <div>
            <div className='review-1'>
            <FontAwesomeIcon icon={faStar} className='star-color'/>
            <FontAwesomeIcon icon={faStar} className='star-color'/>
            <FontAwesomeIcon icon={faStar} className='star-color'/>
            <FontAwesomeIcon icon={faStar} className='star-color'/>
            <FontAwesomeIcon icon={regularStar} className='star-color'/>
            <p>Etiam sapien sagittis diam congue augue massa varius egestas ociis ultrice varius 
						magna tempus an aliquet undo cursus suscipit augue pretium lacinia	</p>
            </div>
            </div>
            <div>
            <div className='review-1'>
            <FontAwesomeIcon icon={faStar} className='star-color'/>
            <FontAwesomeIcon icon={faStar} className='star-color'/>
            <FontAwesomeIcon icon={faStar} className='star-color'/>
            <FontAwesomeIcon icon={faStar} className='star-color'/>
            <FontAwesomeIcon icon={regularStar} className='star-color'/>
            <p>Etiam sapien sagittis diam congue augue massa varius egestas ociis ultrice varius 
						magna tempus an aliquet undo cursus suscipit augue pretium lacinia	</p>
            </div>
            </div>
            <div>
            <div className='review-1'>
            <FontAwesomeIcon icon={faStar} className='star-color'/>
            <FontAwesomeIcon icon={faStar} className='star-color'/>
            <FontAwesomeIcon icon={faStar} className='star-color'/>
            <FontAwesomeIcon icon={faStar} className='star-color'/>
            <FontAwesomeIcon icon={regularStar} className='star-color'/>
            <p>Etiam sapien sagittis diam congue augue massa varius egestas ociis ultrice varius 
						magna tempus an aliquet undo cursus suscipit augue pretium lacinia	</p>
            </div>
            </div>
             
        
        </Slider>  
        

    </div>
  )
}

export default Review