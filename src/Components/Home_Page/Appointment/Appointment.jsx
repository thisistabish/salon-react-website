import React from 'react'
import "./Appointment.css"
import Button from '../Button/Button'
import Women3 from '../../../assets/woman_05.jpg'

const Appointment = () => {
  return (
    <div style={{backgroundColor:"#f8f5f2", paddingTop:"25px"}}>
    <div className='container-6 boxed-width'>
        <div className='appoin-content'>
            <h4>COME, RELAX AND ENJOY</h4>
            <h3 className='font-2'>Place where you will feel peaceful</h3>
            
        <p>Sagittis congue augue egestas integer velna purus purus magna blandit suscipit egestas magna diam ipsum aliquam vitae purus justo lacus ligula ipsum congue tempor undo quisque fusce cursus neque</p>
        <Button  text="BOOK AN APPOINTMENT"/>
        </div>
        
          <img className='img' src={Women3} alt="" />
        

    </div>
    </div>
  )
}

export default Appointment