import React, { useState } from 'react';
import "./Accordion.css"

const Accordion = ({ title, content }) => {
   
        const [isActive, setIsActive] = useState(false);
      
  return (
    <div className="accordion-item">
      <div className="accordion-title" onClick={() => setIsActive(!isActive)}>
        <div className='title'>{title} </div>
        
        <div className='symbol'>{isActive ? '-' : '+'}</div>
        
      </div>
      <hr />
      {isActive && <div className="accordion-content">{content}</div>}
    </div>
  )
}

export default Accordion