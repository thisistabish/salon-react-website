import React from 'react'
import Blog1 from '../../../assets/blog-01.jpg'
import Blog2 from '../../../assets/blog-02.jpg'
import Blog3 from '../../../assets/blog-03.jpg'
import './Blog.css'
const Blog = () => {
  return (
    <div className='blog-sec-p boxed-width'>
        <h4>FROM THE BLOG</h4>
        <h2 className='font-2'>Our Latest News</h2>
        <div className='blog-sec-c'>
            <div className='blog'>
                <div style={{overflow: "hidden"}}>
                <img src={Blog1} alt=""className='zoom' /></div>
                <h4>NEWS | AUGUST 03, 2023</h4>
                <h3 className='font-2'>Quaerat sodales sapien and tempor diam euismod purus</h3>
                <p>Velna purus purus magna ipsum suscipit egestas magna aliquam ipsum vitae...</p>
            </div>
            <div className='blog'>
                <div style={{overflow: "hidden"}}>
            <img src={Blog2} alt="" className='zoom' /></div>
            <h4>TRENDS | JULY 31, 2023</h4>
                <h3 className='font-2'>Luctus vitae and egestas</h3>
                <p>Congue augue sagittis egestas integer velna purus purus magna suscipit...</p>
            </div>
            <div className='blog'>
                <div style={{overflow: "hidden"}}>
            <img src={Blog3} alt="" className='zoom' /></div>
            <h4 >TREATMENT | JULY 18, 2023</h4>
                <h3 className='font-2'>Magna aliquam ipsum a vitae purus justo lacus ligula</h3>
                <p>Congue augue sagittis egestas integer velna purus and magna ipsum suscipit...</p>
            </div>
        </div>

    </div>
  )
}

export default Blog