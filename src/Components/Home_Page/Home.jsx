import React, { useEffect, useState } from "react";
import { useNavigate,useLocation } from "react-router";
import HeroSection from '../../Components/Hero_Section/HeroSection';
import AboutSec from '../../Components/Home_Page/AboutSec/AboutSec';
import Services from '../../Components/Home_Page/Services/Services';
import Appointment from '../../Components/Home_Page/Appointment/Appointment';
import Carousel from '../../Components/Home_Page/Carousel/Carousel';
import Gallery from '../../Components/Home_Page/Gallery/Gallery';
import Offer from '../../Components/Home_Page/Offer/Offer';
import Review from '../../Components/Home_Page/Review/Review';
import SalonMenu from '../../Components/Home_Page/Salon_Menu/SalonMenu';
import Header from '../../Components/Header/Header';
import Location from '../../Components/Home_Page/Location/Location';
import Blog from '../../Components/Home_Page/Blog/Blog';
import Subscribe from '../../Components/Home_Page/Subscribe/Subscribe';
import Follow from '../../Components/Home_Page/Follow/Follow';
import Footer from '../../Components/Footer/Footer'



const Home = () => {
  const navigate = useNavigate()
  const url = useLocation();
  const path = url.pathname;
  console.log(path);
  const [route, setRoute] = useState([]);
  useEffect(() => {
    fetch("/routes.json")
      .then((data) => data.json())
      .then((data) =>{
        const matchRoute = data.find((route) => route.path === path);
        if (!matchRoute) {
          navigate("/404");
          return null;
        }else {
          setRoute(matchRoute)
        }
      });
  }, []);
  return (
    <>
         <Header />
          <HeroSection route={route}/>
          <AboutSec/>
          <Services/>
          <Appointment />
          <Carousel/>
          <Gallery/>
          <Offer route={route}/>
          <Review/>
          <SalonMenu/>
          <Location/>
          <Blog/>
          <Subscribe/>
          <Follow/>
          <Footer/>
        
          </>
  )
}

export default Home