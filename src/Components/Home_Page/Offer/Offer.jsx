import React from 'react'
import './Offer.css'
import Button from '../Button/Button'

const Offer = ({route}) => {
  return (
    <div className='bg-image-2'>
        <div className='align'>
        <h4 >{route?.bannerSection?.subTitle}</h4>
        <h3 className='font-2' dangerouslySetInnerHTML={{__html: route?.bannerSection?.title}}></h3>
        
        <Button text="BOOK AN APPOINTMENT" className="app-button " />
        </div>
    </div>
    
  )
}

export default Offer