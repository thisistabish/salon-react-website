import React from 'react'
import './Follow.css'
import f1 from '../../../assets/f-1.jpg'
import f2 from '../../../assets/f-2.jpg'
import f3 from '../../../assets/f-3.jpg'
import f4 from '../../../assets/f-4.jpg'
import f5 from '../../../assets/f-5.jpg'
import f6 from '../../../assets/f-6.jpg'


const Follow = () => {
  return (
    <div className='follow-sec boxed-width'>
        <h3 className='font-2'>Follow: @reine_studio</h3>
        <div className='follow-img'>
            <div className='f-img'>
                <img src={f1} alt="" />
            </div>
            <div className='f-img'>
            <img src={f2} alt="" />
            </div>
            <div className='f-img'>
            <img src={f3} alt="" />
            </div>
            <div className='f-img'>
            <img src={f4} alt="" />
            </div>
            <div className='f-img'>
            <img src={f5} alt="" />
            </div>
            <div className='f-img'>
            <img src={f6} alt="" />
            </div>
        </div>
    </div>
  )
}

export default Follow