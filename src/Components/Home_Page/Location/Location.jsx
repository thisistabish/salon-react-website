import React from 'react'
import Salon from '../../../assets/salon_02.jpg'
import Salon2 from '../../../assets/salon_03.jpg'
import './Location.css'

const Location = () => {
  return (
    <div className='loc-sec-p boxed-width'>
        <h4 style={{fontSize:"15.64px", color:"#6F6F6F"}}>OUR LOCATIONS</h4>
        <h2 className='font-2' style={{fontSize:'59.8px',color:'#363636'}}>Welcome to Reine</h2>
        <div className='loc-sec-child'>
            <div className='loc-sec-data ' >
              <div style={{overflow:'hidden'}}>
                <img src={Salon} alt="" className='zoom' /></div>
                <h2>Visit Reine Wilshire</h2>
                <h3>8721 Central Ave, Los Angeles, CA 90036</h3>
                <h4>est. 2018 &nbsp;  <b>Learm More | Book Now</b>
</h4>
            </div>
            <div className='loc-sec-data' >
            <div  style={{overflow:'hidden'}}>
                <img src={Salon2} alt="" className='zoom'/></div>
                <h2>Visit Reine Westwood</h2>
                <h3>8493 Sunset Blvd, Los Angeles, CA 90069</h3>
                <h4>est. 2021 &nbsp;  <b>Learm More | Book Now</b>
</h4>
                
            </div>
            
        </div>
    </div>
  )
}

export default Location