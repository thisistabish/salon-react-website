import React from 'react'

import './Gallery.css'
import image from '../../../assets/woman_04.jpg'
import image2 from '../../../assets/woman_01 (1).jpg'
import image3 from '../../../assets/woman_03.jpg'
import image4 from '../../../assets/woman_02.jpg'
import image5 from '../../../assets/woman_05 (1).jpg'
import image6 from '../../../assets/woman_06.jpg'
import Button from '../Button/Button'



const Gallery = () => {
  return (
    <div className='content-container'>
        <h4>BE A MORE PERFECT</h4>
        <h2 className='font-2'>Redefine Your Beauty</h2>
    <div className='gallery-container'>
        <div className='first-column img-wid'>
            <img className=' zoom' src={image} alt="" />
        </div >
        <div className='second-column img-wid'>
            <img className=' zoom' src={image2} alt="" />
        </div>
        <div className='third-column img-wid'>

            <div className='third-1' ><img className=' zoom '  src={image3} alt="" /></div>
            <div className='third-1'><img className=' zoom' src={image4} alt="" /></div>
        </div>
        <div className='fourth-column img-wid'>
            <div className='third-1'>
            <img className='zoom' src={image5} alt="" />
            </div>
            <div className='third-1'>
            <img className='zoom' src={image6} alt="" />
            </div>
        </div>
    </div>
    <Button text="VIEW OUR GALLERY" className="gallery-btn" />
    </div>
  )
}

export default Gallery