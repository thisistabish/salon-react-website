import React from "react";
import Slider from "react-slick";
import './Carousel.css'
import brand1 from "../../../assets/brand-1.png"
import brand2 from '../../../assets/brand-3.png'
import brand3 from '../../../assets/brand-4.png'
import brand4 from '../../../assets/brand-6.png'
import brand5 from '../../../assets/brand-7.png'
import brand6 from '../../../assets/brand-8.png'
import brand7 from '../../../assets/brand-9.png'

export default function SimpleSlider() {
  var settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay:true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }
    ]
  };
  return (
    <div className="carousel-container">
    <Slider {...settings} className="boxed-width">
      
      <div className="brand-img" >
        <img src={brand1} alt="" />
      </div>
      <div className="brand-img">
        <img src={brand2} alt="" />
      </div >
      <div className="brand-img">
        <img src={brand3} alt="" />
      </div>
      <div className="brand-img">
        <img src={brand4} alt="" />
      </div>
      <div className="brand-img" >
        <img src={brand5} alt="" />
      </div>
      <div className="brand-img">
        <img src={brand6} alt="" />
      </div>
      <div className="brand-img">
       <img src={brand7} alt="" />
      </div>
      
     
    </Slider>
    </div>
  );
}