import React from 'react'
import Accordion from '../Accordion/Accordion'
import Image from '../../../assets/woman_02 (2).jpg'
import Facial from '../../../assets/facial-treatment.png'
import Wax from '../../../assets/wax (1).png'
import Beauty from '../../../assets/beauty.png'
import Nail from '../../../assets/nail-polish.png'
import Women from '../../../assets/woman_01.jpg'
import './AboutSec.css'
import Button from '../Button/Button'

const AboutSec = () => {

  const accordionData = [
    {
      title: 'Certified Stylists',
      content: `Nemo ipsam magna volute placerat a turpis ipsum purus sapien ultrice ipsum aliquam congue dolor`
    },
    {
      title: '100% Organic Cosmetics',
      content: `Nemo ipsam magna volute placerat a turpis ipsum purus sapien ultrice ipsum aliquam congue dolor`
    },
    {
      title: 'Easy Online Booking',
      content: `Nemo ipsam magna volute placerat a turpis ipsum purus sapien ultrice ipsum aliquam congue dolor`
    }
  ];

  return (

    // first section
    <div style={{ backgroundColor: "#f8f5f2" }}>
      <div className='container-2 boxed-width'>

        <div className='content '>
          <h4>MIND BODY AND SOUL</h4>
          <h2 className='font-2'>Luxury salon where you will feel unique</h2>
          <p >Sagittis congue augue aegestas integer velna purus purus magna libero suscipit and egestas magna aliquam ipsum vitae purus justo lacus ligula ipsum primis cubilia donec undo augue luctus vitae egestas a molestie donec libero sapien dapibus congue tempor undo quisque and fusce cursus neque blandit fusce aliquam nulla lacinia</p>
          <Button text='DISCOVER MORE' />
        </div>
        <div className='image-section '>
          <img src={Image} alt="" />
        </div>
        
        {/* first section completed */}


      </div>
      {/* first complete */}
      {/* second Section */}

      <div className='container-3 boxed-width'>
        <h3>INDULGE YOURSELF</h3>
        <h1 className='font-2'>Your Secret Place of Beauty</h1>
        <p>Congue augue sagittis egestas integer velna purus purus magna nec suscipit and egestas magna aliquam ipsum vitae purus justo lacus ligula and ipsum lacinia primis cubilia</p>
        <div className='grid-container '>
          <div className='grid-item'>
            <img src={Facial} alt="" />
            <h4 className='font-2'>Facials</h4>
            <p>Sagittis congue augue egestas integer diam purus magna and egestas magna suscipit</p>
          </div>
          <div className='grid-item'>
            <img src={Wax} alt="" />
            <h4 className='font-2'>Waxing</h4>
            <p>Sagittis congue augue egestas integer diam purus magna and egestas magna suscipit</p>
          </div>

          <div className='grid-item'>
            <img src={Beauty} alt="" />
            <h4 className='font-2'>Make-Up
            </h4>
            <p>Sagittis congue augue egestas integer diam purus magna and egestas magna suscipit</p>
          </div>


          <div className='grid-item'>
            <img src={Nail} alt="" />
            <h4 className='font-2'>Nails</h4>
            <p>Sagittis congue augue egestas integer diam purus magna and egestas magna suscipit</p>
          </div>

        </div>
      </div>
      {/* second complete */}
      {/* third section */}

      <div className='boxed-width'>
      <div class="container-4">
        <div class="row">
          <div class="row-child">
            <img class="pic" src={Women} alt='' />
            <div class="content-1">
              <h4>NATURALLY YOU</h4>
              <h3 className='font-2'>Look more natural with Reine studio</h3>

              <div className='accordion'>


                {accordionData.map(({ title, content }) => (
                  <Accordion title={title} content={content} />


                ))}

              </div>
            </div>
          </div>
        </div>
        </div>
        {/* third complete */}
        {/* fourth section */}
        <div className='container-5'>
          <div className='content-2'>
            <h4>TIME SCHEDULE</h4>
            <h3 className='font-2'>Working Hours</h3>
            <p>Nemo ipsam egestas volute turpis varius ipsum egestas purus diam ligula sapien ultrice sapien tempor aliquam tortor ipsum and augue turpis quaerat aliquet congue and molestie magna in congue undo aliquet congue ultrices quaerat purus justo</p>

          </div>
          <div className='table'>
            <tr className='row-2'>
              <th>Mon – Wed</th>
              <th>-</th>
              <th>10:00 AM - 9:00 PM</th>
            </tr>
            <hr />
            <tr className='row-2'>
              <th>Mon – Wed</th>
              <th>-</th>
              <th>10:00 AM - 9:00 PM</th>
            </tr>
            <hr />
            <tr className='row-2'>
              <th>Mon – Wed</th>
              <th>-</th>
              <th>10:00 AM - 9:00 PM</th>
            </tr>
            <hr />
            <tr className='row-2'>
              <th>Mon – Wed</th>
              <th>-</th>
              <th>10:00 AM - 9:00 PM</th>
            </tr>
          </div>
        </div>
        


      </div>
      <div className='bg-image'></div>


    </div>







  )
}

export default AboutSec