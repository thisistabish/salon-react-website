import React from 'react'
import "./Subscribe.css"
import Button from '../Button/Button'

const Subscribe = () => {
  return (
    <div className='sub-img boxed-width'>
        <h2>Join Our Newsletter</h2>
        <h4>Receive beauty and wellness insights, events and latest offers!</h4>
        <div className='email-input'>
        <input type="email" placeholder='Enter your email'/>
       
        <Button text="SUBSCRIBE" className="app-button btn-position" />
        
        </div>
        
    </div>
  )
}

export default Subscribe