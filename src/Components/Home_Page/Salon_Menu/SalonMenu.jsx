import React from 'react'
import Women4 from "../../../assets/woman_03 (1).jpg"
import './SalonMenu.css'
import Button from '../Button/Button'
const SalonMenu = () => {
  return (
    <div className='boxed-width'>
    <div style={{backgroundColor:'#f8f5f2', margin:'auto'}}>
      <div className='banner-sec-p boxed-width'>
      <div className='banner-sec'>
        <div className='banner-sec-child'>
          <h4>YOU ARE BEAUTIFUL</h4>
          <h3 className='font-2'>Unleash your inner beauty with Reine</h3>
          <ul >
            <li>Aliquam vitae molestie quisque sapien diam purus egestas quaerat an aliquet molestie ipsum</li>
            <li>Sagittis congue augue magna volutpat porta mauris purus and egestas ipsum suscipit quaerat augue</li>
            <Button text="SALON MENU" additionalClasses="custom-class" />
          </ul>
         
        </div>
        <img src={Women4} alt='' />
      </div>
    </div>
    </div>
    </div>
  )
}

export default SalonMenu