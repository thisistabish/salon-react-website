import React from 'react'
import './PriceList.css'

const PriceList= ({route}) => {
  return (
    <div className="boxed-width columns-2 gap-16">
      {route?.serviceRates?.map((item, index) => (
    <div key={index} className='max-w-screen-xl m-auto pt-24 '	>
    <h1 className='text-4xl font-2 pb-8 font-normal	text-gray-900'>{item?.heading}</h1>
    <ol>
    {item?.priceList?.map((item2, index) => (
  <li key={index} >
    <div class="text-div text-xl">
      <span class="text-span ">{item2?.title}</span>
      <span class="text-span pull-right ">{item2?.price}</span><br/>
      <font className='text-gray-600 font-normal	text-lg		'>{item2?.subTitle}</font>
    </div>
  </li>))}
</ol>
</div>))}
</div>
  )
}

export default PriceList;