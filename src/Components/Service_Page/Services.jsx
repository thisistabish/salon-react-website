
import React, { useEffect, useState } from "react";
import { useNavigate,useLocation } from "react-router";
import HeroSection from "../Hero_Section/HeroSection";
import Header from '../Header/Header'
import PriceList from "./PriceList/PriceList";
import Gallery from "../Home_Page/Gallery/Gallery";
import Offer from "../Home_Page/Offer/Offer";

const Services = () => {
  
  const navigate = useNavigate()
  const url = useLocation();
  const path = url.pathname;
  console.log(path);
  const [route, setRoute] = useState([]);
  useEffect(() => {
    fetch("/routes.json")
      .then((data) => data.json())
      .then((data) =>{
        const matchRoute = data.find((route) => route.path === path);
        if (!matchRoute) {
          navigate("/404");
          return null;
        }else {
          setRoute(matchRoute)
        }
      });
  }, []);
    
  return (
  <div>
    <Header/>
    <HeroSection route={route}/>
    <PriceList route={route}/>
    <Gallery/>
  <Offer route={route}/>
    </div>);
    
  }
export default Services;
