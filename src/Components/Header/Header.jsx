
import './Header.css'
import originalLogo from '../../assets/logo-white.png'
import scrolledLogo from '../../assets/logo-black.png'
import { Link } from 'react-router-dom'
import React, { useState, useEffect } from 'react';

const Header = () => {
  const [scrolled, setScrolled] = useState(false);

  const handleScroll = () => {
    if (window.scrollY > 0) {
      setScrolled(true);
    } else {
      setScrolled(false);
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);
  return (
    <div>
        <div className={`header${scrolled ? ' scrolled' : ''}`}>
      <div className='logo'>
      <img src={scrolled ? scrolledLogo : originalLogo} alt="" />
       <div className='nav-links'>
        <ul>
          <li ><Link to={"/"}>Home</Link>
          
          </li>
          <li>
            <Link to={"/services"}>Services</Link>
          </li>        
          <li>Contact</li>
        
        
          
          
          
          </ul>
          <button>Book Online</button>
          
        
      </div>
      </div>
      
    </div>
    </div>
  )
}

export default Header