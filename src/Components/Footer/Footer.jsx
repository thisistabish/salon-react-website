import React from 'react'
import'./Footer.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInstagram, faSquareFacebook, faTwitter } from '@fortawesome/free-brands-svg-icons'

const Footer = () => {
  return (
    <div className='footer-sec boxed-width'>
        <div className='footer-grid'>
            <div className='foo-g-1'>
                <h3 className='font-2'>Our Location</h3>
                <p>Reine Studio<br/>
                Los Angeles, 8721 M<br/>
                Central Avenue, CA 90036</p>
            </div>
            <div className='foo-g-1'>
            <h3 className='font-2'>Our Location</h3>
            <p>phone: +12 9 8765 4321<br/>
                
            hello@yourdomain.com</p>
            
            <div style={{paddingTop:"22px"}} >
            <FontAwesomeIcon icon={faSquareFacebook} className='social-icons' style={{color:"#6f6f6f", fontSize:"30px"}} />&nbsp;&nbsp; 
            <FontAwesomeIcon icon={faTwitter}style={{color:"#6f6f6f", fontSize:"30px"}}/>&nbsp;&nbsp;
            <FontAwesomeIcon icon={faInstagram}style={{color:"#6f6f6f", fontSize:"30px"}}/>
            </div>
            
            
            </div>
            <div className='foo-g-1'><h3>Working Hours</h3>
                <p>Mon-Fri: 10:00AM - 9:00PM<br/>

                    Saturday: 10:00AM - 7:00PM<br/>

                    Sunday: 10:00PM - 7:00PM</p></div>
        </div>
        <hr style={{marginTop:"80px"}}/>
        <p className='copy'>© 2023 Reine. All Rights Reserved</p>
    </div>
  )
}

export default Footer